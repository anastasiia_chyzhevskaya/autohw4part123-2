package com.company;

import java.util.Scanner;

public class Task2 {
    /*A Универы выставляют оценки по такой шкале
    a. Ниже 25 - F
    b. 25 to 45 - E
    c. 45 to 50 - D
    d. 50 to 60 - C
    e. 60 to 80 - B
    f. Выше 80 - A

    Введите оценку и распечатайте ее grade (A, B. F …)
    */

    public static void rate (){
        System.out.println();
        int rate = getInt("Введите оценку > 0 ");
        if (rate < 25){
            System.out.println(rate + " - соответствует оценке F (т.е. ниже 25)");
        } else if (rate < 45){
            System.out.println(rate + " - соответствует оценке E (т.е. ниже 45)");
        } else if (rate < 50){
            System.out.println(rate + " - соответствует оценке D (т.е. ниже 50)");
        } else if (rate < 60) {
            System.out.println(rate + " - соответствует оценке C (т.е. ниже 60)");
        } else if (rate < 80) {
            System.out.println(rate + " - соответствует оценке B (т.е. ниже 80)");
        } else if (rate > 80) {
            System.out.println(rate + " - соответствует оценке A (т.е. выше 80)");
        } else {
            System.out.println("Smth went worng");
        }
    }
    private static int getInt(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }
}
